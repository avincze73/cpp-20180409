// Container.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <algorithm>
#include <iostream>
#include <memory>
#include <string>
#include <iterator>
#include <list>
#include <set>
#include <map>
#include <functional>
using namespace std;


template <typename C>
auto sum(C container)
{
	auto i = container.begin();
	decltype(i) j = container.end();
	typename C::value_type sum = 0;
	//decltype((*i)) sum = 0;
	for (; i != j; ++i)
	{
		cout << *i << endl;
		sum += *i;
	}
	return sum;
}


class Employee
{
	string name;
	string title;
public:
	Employee(string name, string title)
		:name(name), title(title){}
};

int main()
{
	vector<int> v{1,2,3,4,5,6,7};
	vector<int>::iterator j = v.begin();
	cout << sum(v) << endl;
	vector<int> v2(v.begin() + 1, v.end()-1);
	for_each(v2.rbegin(), v2.rend(), [](auto a) {cout << a; });
	
	auto jj = v2.begin() + 2;

	vector<int> v3;
	v3.reserve(800000);
	v2.front() = 2;
	v2[0] = 2;
	v2.insert(v2.begin(), v.begin() + 2, v.end() - 2);
	v2.insert(v2.begin() + 2, 44);
	
	//*jj = 88;


	for (auto i = v.begin(); 
		i != v.end(); ++i)
	{
		int a = *i;
	}


	{
		vector<shared_ptr<int>> vup;

	
		vup.push_back(shared_ptr<int>(new int(1)));
		vup.push_back(shared_ptr<int>(new int(2)));
		vup.push_back(shared_ptr<int>(new int(3)));
		for_each(vup.begin(), vup.end(),
			[](shared_ptr<int> p) { cout << *p; });
	}


	vector<Employee> employeeVector;
	employeeVector.emplace_back("aa", "bb" );





	vector<int> v4;
	vector<int>::iterator v4i = v4.end();
	//*v4i = 10;

	//back insert iterator
	back_insert_iterator<vector<int>> bi4(v4);
	/**bi4 = 2;
	*bi4 = 3;
	*bi4 = 4;*/



	/*vector<int> v5{ 1,2,3,4,5,6,7,8,9,10 };
	vector<int> v6;
	copy_if(v5.begin(), 
		v5.end(), 
		back_inserter(v6), [](auto x) {return !(x % 2); }
	);*/

	list<int> l7{1};
	list<int>::iterator l7i = l7.begin();
	inserter(l7, l7i) = 2;
	/*copy_if(v5.begin(),
		v5.end(),
		inserter(l7, l7i), [](auto x) {return !(x % 2); }
	);*/



	//set
	
	set<int> s1{ 8,6,5,7,1,2,3,8 };
	pair<decltype(s1.begin()), bool> ret = s1.insert(1);
	cout << *ret.first << " " << ret.second<< endl;

	for_each(s1.begin(), s1.end(),
		[](auto s) { cout << s << endl; });

	map<int, string> m1;
	m1[2] = "aaaa";
	
	multimap<int, string> m2;







    return 0;
}

