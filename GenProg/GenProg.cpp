// GenProg.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Header.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <functional>
#include <string>

using namespace std;

struct MyPrint
{
	void operator()(int i)
	{
		cout << i << endl;
	}
};

template <typename T>
class Average
{
	T sum;
	int count;
public:
	Average() :sum(0), count(0)
	{
	}
	void operator()(int i)
	{
		count++;
		sum += i;
	}
	double getAverage()
	{
		return static_cast<double>(sum) / count;
	}
};

template <typename T>
Average<T> make_average(T)
{
	return Average<T>();
}


template <typename T>
struct Greater : binary_function<T, T, bool>
{
	bool operator()(T a, T b) const
	{
		return a > b;
	}
};


class Employee
{
	string name;
public:
	Employee(string name) :name(name) {

	}
	string getName() { return name; }
	void info() {
		cout << name << endl;
	}

	void info1(string a) {
		cout << a << name << endl;
	}
	
	void info2(string a, string b) {
		cout << a  << b << name << endl;
	}

	void info3(string a, string b, string c) {
		cout << a << b << c << name << endl;
	}
};

void h(int a, int b, int c, int d)
{
	cout << a << " " << b << " " << c << " "
		<< d << endl;
}




function<int(int, int)>
getPolicy(char ch)
{
	switch (ch)
	{
	case 'm':
		return [](int a, int b)->int {return a * b; };
		break;
	case 'a':
		return add;
		break;
	default:
		return Add();
		break;
	}
}


int main()
{
	int a[] = {1,2,3,4,5,6,7,8,9,10};

	cout << accum(a, 10, 0) << endl;
	
	cout << accum(a, 10, 0, add) << endl;
	cout << accum(a, 10, 1, multiply) << endl;

	cout << accum(a, 10, 0, Add()) << endl;
	cout << accum(a, 10, 1, Multiply()) << endl;

	cout << accum(a, 10, 0, Add(), Positive()) << endl;
	cout << accum(a, 10, 1, Multiply(), Even()) << endl;

	cout << accum(a, a + 10, 0, 
		Add(), Positive()) << endl;
	
	cout << accum(a, a + 10, 1, 
		Multiply(), Even()) << endl;

	
	cout << accum2(a, a + 10,
		0, plus<int>(), 
		not1(bind1st(greater<int>(), 0))) << endl;

	cout << accum2(a, a + 10,
		0, plus<int>(),
		bind2nd(not2(less<int>()), 0)) << endl;



	cout << accum2(a, a + 10,
		0, plus<int>(),
		bind2nd(not2(Greater<int>()), 0)) << endl;






	double d[] = {1.1,2.2,3.3,4.4};

	cout << for_each(a, a + 10, 
		make_average(*a))
		.getAverage() << endl;

	cout << for_each(d, d + 4, 
		make_average(*d))
		.getAverage() << endl;

	int* p = a;
	while ((p = find_if(p, a + 10, Even())) 
		!= a + 10)
	{
		cout << *p << endl;
		++p;
	};

	ofstream os("d:\\temp\\a.out");
	//os.open();

	int b[10];
	copy_if(a, a + 10,
		ostream_iterator<int>(os, "\n"),
		Even());
	os.close();
	
	copy_if(a, a + 10, b, Even());
	



	Employee e[] = {
		Employee("aaa"),
		Employee("bbb"),
		Employee("ccc"),
		Employee("ddd")
	};
	for_each(e, e + 4,
		mem_fun_ref(&Employee::info)
	);


	/*for_each(e, e + 4,
		bind(&Employee::info)
	);*/

	Employee* e2[] = {
		new Employee("aaa"),
		new Employee("bbb"),
		new Employee("ccc"),
		new Employee("ddd")
	};
	for_each(e2, e2 + 4,
		mem_fun(&Employee::info)
	);

	/*for_each(e2, e2 + 4,
		mem_fn(&Employee::info)
	);
	for_each(e, e + 4,
		mem_fn(&Employee::info)
	);*/


	bind(h, std::placeholders::_2, 
		std::placeholders::_1,
		std::placeholders::_3, 
		std::placeholders::_4)
		(1, 2, 3, 4);
	for_each(e2, e2 + 4,
		bind(&Employee::info1, 
			std::placeholders::_1,
			"aaa")
	);
	string out = "hello";
	for_each(e2, e2 + 4,
		bind(&Employee::info2,
			std::placeholders::_1,
			"aaa", out)
	);
	for_each(e, e + 4,
		bind(&Employee::info, std::placeholders::_1)
	);

	function<int(int, int)> policy
		= [](int a, int b)->int {return a + b; };
	function<bool(int)> predicate
		= [](int a)->bool {return a > 0; };

	cout << accum2(a, a + 10, 0,
		getPolicy('a'),
		predicate
		) << endl;



	function<int(int, int)> aaa = add;

	function<int(int, int)> bbb = Add();

	function<int(int, int)> ccc = [](int a, int b) {return a + b; };

	function<int(int, int)> ddd =
		bind(add, std::placeholders::_2, std::placeholders::_1);

	ddd(1, 2);
	ccc(2, 3);
	bbb(3, 3);

	return 0;


}

