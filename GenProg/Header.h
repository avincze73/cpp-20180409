#pragma once

int accum(int*, int, int);
typedef int(*AccumPolicy)(int, int);
int accum(int*, int, int, AccumPolicy);
int add(int, int);
int multiply(int, int);
struct Operation;
int accum(int*, int, int, Operation&);
struct Condition;
int accum(int*, int, int, Operation&, Condition&);
int accum(int*, int*, int, Operation&, Condition&);



struct Operation
{
	virtual int operator()(int, int) = 0;
};

struct Add : Operation
{
	virtual int operator()(int a, int b)
	{
		return a + b;
	}
};

struct Multiply : Operation
{
	virtual int operator()(int a, int b)
	{
		return a * b;
	}
};

struct Condition
{
	virtual bool operator()(int) = 0;
};

struct Even : Condition
{
	virtual bool operator()(int a)
	{
		return !(a % 2) ;
	}
};

struct Positive : Condition
{
	virtual bool operator()(int a)
	{
		return a > 0;
	}
};

template <typename T,
	typename I, typename O, typename C>
	T accum2(I begin, I end, T init, O oper, C cond)
{
	for (; begin != end; ++begin)
	{
		if (cond(*begin))
		{
			init = oper(init, *begin);
		}
	}
	return init;
}

struct Substract
{
	int operator()(int a, int b)
	{
		return a - b;
	}
};
