#include "Header.h"

int accum(int* a, int length, int init)
{
	for (size_t i = 0; i < length; i++)
	{
		init += *(a + i);
	}
	return init;
}

int accum(int *a, int length, 
	int init, AccumPolicy oper)
{
	for (size_t i = 0; i < length; i++)
	{
		init = oper(init, *(a + i));
	}
	return init;
}

int add(int a, int b)
{
	return a + b;
}

int multiply(int a, int b)
{
	return a * b;
}

int accum(int * a, int length, 
	int init, Operation& oper)
{
	for (size_t i = 0; i < length; i++)
	{
		init = oper(init, *(a + i));
	}
	return init;
}

int accum(int * a, int length, 
	int init , Operation & oper, 
	Condition & cond)
{
	for (size_t i = 0; i < length; i++)
	{
		if (cond(*(a+i)))
		{
			init = oper(init, *(a + i));
		}
	}
	return init;
}

int accum(int *begin, int *end, 
	int init, Operation &oper, 
	Condition &cond)
{
	for (; begin != end; ++begin)
	{
		if (cond(*begin))
		{
			init = oper(init, *begin);
		}
	}
	return init;
}
