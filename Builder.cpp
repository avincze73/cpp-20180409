// Builder.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
using namespace std;

class Computer
{
public:
	class Builder
	{
		friend class Computer;
	private:
		string processor2, memory2, hdd2, display2;
	public:
		Builder(string hdd):hdd2(hdd){}
		Builder& processor(string processor) { processor2 = processor; return *this; }
		Builder& memory(string memory) { memory2 = memory; return *this; }
		Builder& display(string display) { display2 = display; return *this; }
		Computer* build(){return new Computer(this);}
	};
private:
	string processor, memory, hdd, display;
	Computer(Builder* builder)
		:processor(builder->processor2), memory(builder->memory2), display(builder->display2), hdd(builder->hdd2){}
};

int main()
{
	Computer* c = Computer::Builder("1TB").processor("i7")
		.memory("16GB").display("24col").build();
    return 0;
}

