#pragma once
#include <iostream>
#include <string.h>

int maxx(int a, int b);

template <typename T>
T maxx(T a, T b)
{
	//int c = T::hello;
	//int b = a.world();
	//int d = a + b;


	std::cout << "max_t" << std::endl;
	return a > b ? a : b;
}

template <typename T>
T* maxx(T* a, T* b)
{
	std::cout << "max_t_*" << std::endl;
	return *a > *b ? a : b;
}



template<>
const char* maxx(const char* a, const char* b)
{
	std::cout << "max_t_const_char_*" << std::endl;
	return strcmp(a, b) ? a : b;
}


