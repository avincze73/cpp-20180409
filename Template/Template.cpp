// Template.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Header.h"
#include <iostream>
using namespace std;

class IntHolder
{
private:
	int size;
	int* a;
	//IntHolder(const IntHolder&);
public:
	IntHolder(int size)
		:size(size), a(new int[size]) {

	}
	~IntHolder() {
		delete[] a;
	}
	//IntHolder(const IntHolder&) = delete;
	/*IntHolder(const IntHolder& v)
		:size(v.size), a(new int[size])
	{
		cout << "cctor" << endl;
		for (size_t i = 0; i < size; i++)
		{
			a[i] = v.a[i];
		}
	}*/

	/*IntHolder(IntHolder&& v)
	{
		cout << "mctor" << endl;
		size = v.size;
		a = v.a;
		v.a = 0;
	}*/
};


int maxx(int a, int b)
{
	cout << "max" << endl;
	return a > b ? a : b;
}

void f(IntHolder ih)
{

}



IntHolder g()
{
	return IntHolder(100);
}

int main()
{
	IntHolder ih(100);
	f(ih);
	f(std::move(g()));

/*

	const char * a = "aaa";
	const char * b = "bbb";
	int i = 1, j = 2;
	int *pi = &i, *pj = &j;
	cout << maxx(1, 2) << endl;

	cout << maxx(1., 2.) << endl;
	cout << maxx(a, b) << endl;
	cout << maxx(pi, pj) << endl;
*/

    return 0;
}

