#pragma once

class A
{
	int x, y;
public:
	A();
	~A();
	virtual void f() {}
	virtual void g() {}
};

class B : public A
{
	int z;
public:
	virtual void h(){}
};

