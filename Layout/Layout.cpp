// Layout.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "A.h"
#include "C.h"
#include <iostream>
using namespace std;

typedef  void (A::*ff)(); 

int main()
{
	C c;
	C* pc = &c;
	int* p =(int*) pc;
	//ff f = (ff)((void*)(p));

	++p;
	*(p) = 11;
	cout << *p << endl;
	++p;
	cout << *p << endl;

    return 0;
}

