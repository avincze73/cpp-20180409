// Allocator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <list>
#include <iostream>
#include <memory>
#include "myalloc.hpp"
using namespace std;

template <class T>
struct SimpleAllocator {
	typedef T value_type;
	SimpleAllocator(/*ctor args*/);
	template <class U>
	SimpleAllocator(const SimpleAllocator<U>& other);
	//allocates storage suitable for n objects of type T, but does not construct them. May throw exceptions.
	T* allocate(std::size_t n);
	//deallocates storage pointed to ptr
	void deallocate(T* p, std::size_t n);
};

template <class T, class U>
bool operator==(const SimpleAllocator<T>&, const SimpleAllocator<U>&);
template <class T, class U>
bool operator!=(const SimpleAllocator<T>&, const SimpleAllocator<U>&);


void myalloc_drive()
{
	// create a vector, using MyAlloc<> as allocator
	std::vector<int, MyLib::MyAlloc<int> > v;

	// insert elements
	// - causes reallocations
	v.push_back(42);
	v.push_back(56);
	v.push_back(11);
	v.push_back(22);
	v.push_back(33);
	v.push_back(44);
}


int main()
{
	vector<int> i;
	list<int> j;
	if (i.get_allocator() == j.get_allocator())
	{
		cout << "same allocator" << endl;
	}
	cout <<  std::uses_allocator<vector<int>, SimpleAllocator<double>>::value  << endl;
	//MyLib::MyAlloc<int>::rebind<double>::other;
	//MyLib::MyAlloc<int>::rebind<long long>::other;
	myalloc_drive();
    return 0;
}

