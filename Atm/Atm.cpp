// Atm.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

 
class Detector
{
	int value;
	int count = 0;
	Detector* next;
public:
	//virtual ~Detector()  = 0;
	Detector(Detector* next, int value)
		:next(next), value(value)
	{

	}
	void check(int i)
	{
		count = i / value;

		if (next)
		{
			next->check(i%value);
		}
	}

	void info() {
		std::cout << value << " " << count << std::endl;
		if (next)
		{
			next->info();
		}
	}
};

class D20000 : public Detector
{
public:
	~D20000(){}
	D20000(Detector* next)
		:Detector(next, 20000){}
};
//d1reportSingleClassLayoutC 
class D10000 : public Detector
{
public:
	virtual ~D10000() {}
	D10000(Detector* next)
		:Detector(next, 10000) {}
};

class D5000 : public Detector
{
public:
	~D5000() {}
	D5000(Detector* next)
		:Detector(next, 5000) {}
};

class D2000 : public Detector
{
public:
	~D2000() {}
	D2000(Detector* next)
		:Detector(next, 2000) {}
};

class D1000 : public Detector
{
public:
	~D1000() {}
	D1000(Detector* next)
		:Detector(next, 1000) {}
};


class D500 : public Detector
{
public:
	~D500() {}
	D500(Detector* next)
		:Detector(next, 500) {}
};

int main()
{
	D500 d500(0);
	D1000 d1000(&d500);
	D2000 d2000(&d1000);
	D5000 d5000(&d2000);
	D10000 d10000(&d5000);
	D20000 d20000(&d10000);
	
	d20000.check(104500);
	d20000.info();

    return 0;
}

