// Anagrams.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <map>
#include <vector>
#include <string>
#include <functional>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <fstream>
using namespace std;

vector<string> words;

map<int, int> example1()
{
	cout << "example1" << endl;
	map<int, int> result;
	for_each(words.begin(), words.end(),
		[&result](auto w){result[w.length()]++;}
		);
	return result;
}


map<char, int> example2()
{
	cout << "example2" << endl;
	map<char, int> result;
	for_each(words.begin(), words.end(),
		[&result](auto w) {
		for_each(w.begin(), w.end(),
			[&result](auto c) {result[c]++; }
		);
	}
	);
	return result;
}

int example3()
{
	cout << "example3" << endl;
	int result = 0;
	for_each(words.begin(), words.end(),
		[&result](auto w) {result+=w.length(); }
	);
	return result;
}


tuple<int, int, double> example456()
{
	cout << "example4" << endl;
	map<int, int> result;
	int sumLength = 0;
	for_each(words.begin(), words.end(),
		[&result, &sumLength](auto w) {result[w.length()]++; sumLength += w.length(); }
	);
	tuple<int, int, int> res;
	std::get<0>(res) = (*result.begin()).first;
	std::get<1>(res) = (*result.rbegin()).first;
	std::get<2>(res) = 0;
	
	std::get<2>(res) = (double)sumLength / words.size();
	return res;
}

void init()
{
	ifstream file("d:\\Sprint\\Cpp\\Trainings\\20180409\\sample\\dictionary.txt");
	copy(istream_iterator<std::string>(file),
		istream_iterator<std::string>(),
		back_inserter(words));
}

template <typename T>
void printMap(T container)
{
	for_each(container.begin(), container.end(),
		[](auto e) {cout << e.first << "," << e.second << endl; });

}


vector<string> example7(string word)
{
	cout << "example7" << endl;
	vector<string> result;
	sort(word.begin(), word.end());
	copy_if(words.begin(), words.end(),
		back_inserter(result),
		[word](auto w) {
		string ordered = w; 
		sort(ordered.begin(), ordered.end()); 
		return word == ordered;
	}
		
		);
	return result;
}

map<int, int> example9()
{
	cout << "example9" << endl;
	map<string, int> map1;
	for_each(words.begin(), words.end(),
		[&map1](auto w) {
		string ordered = w;
		sort(ordered.begin(), ordered.end());
		map1[ordered]++;

	}
		);

	map<int, int> result;
	for_each(map1.begin(), map1.end(),
		[&result](auto m) {
		result[m.second]++;
	}
		);
	return result;
}

int main()
{
	init();
	
	map<int, int> m1 = example1();
	printMap(m1);
	

	map<char, int> m2 = example2();
	printMap(m2);

	cout << example3() << endl;

	tuple<int, int, double> e456 = example456();
	cout << std::get<0>(e456) << ","
		<< std::get<1>(e456) << ","
		<< std::get<2>(e456) << endl;

	vector<string> e7 = example7("secure");
	copy(e7.begin(), e7.end(),
		ostream_iterator<string>(cout,",")
		);
	cout << endl;




	map<int, int> m9 = example9();
	printMap(m9);

	map<bool, int> m10;
	for_each(m9.begin(), m9.end(),
		[&m10](auto m) {
		m10[m.first != 1] += m.second;;
	}
		);
	cout << "example10" << endl;
	printMap(m10);

	cout << "example8" << endl;

	for_each(m9.begin(), m9.end(),
		[](auto m) {
		if (m.first==3)
		{
			cout << m.second << endl;
		}
	}
		);


	return 0;
}

