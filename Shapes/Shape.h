#pragma once
#include <string>
using std::string;

class Shape
{
	string n;
public:
	Shape();
	virtual  ~Shape();
	string name();

	virtual double area() = 0;
	virtual double perim() = 0;
protected:
	Shape(string n);

};

