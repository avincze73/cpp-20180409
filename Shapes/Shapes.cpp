// Shapes.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Square.h"
#include <iostream>

void info(Circle circle)
{
	std::cout << "Circle: " << circle.perim() << ","
		<< circle.area() << std::endl;
}

void info(Rectangle circle)
{
	std::cout << "Rectangle: " << circle.perim() << ","
		<< circle.area() << std::endl;
}

void info(Square circle)
{
	std::cout << "Square: " << circle.perim() << ","
		<< circle.area() << std::endl;
}


void info(Shape* circle)
{
	std::cout << circle->name() << circle->perim() << ","
		<< circle->area() << std::endl;
}

int main()
{
	Shape* circle = new Circle(3.);
	Shape* rectangle = new Rectangle(3., 4.);
	Shape* square = new Square(3.);
	info(*((Circle*)circle));
	info(*static_cast<Rectangle*>(rectangle));
	info(*static_cast<Square*>(square));
	info(circle);
	info(rectangle);
	delete circle;
	delete rectangle;
	delete square;


	Shape* shapes[] = {new Circle(1.), 
		new Rectangle(3.,4.),
		new Square(3.)
	};

	for (size_t i = 0; i < 3; i++)
	{
		/*if (typeid(*shapes[i])==typeid(Circle))
		{

		} else if (typeid(*shapes[i]) == typeid(Rectangle))
		{

		}*/
		info(shapes[i]);
	}















	int* vptr = reinterpret_cast<int*>(circle);
	++vptr;
	double* radius = reinterpret_cast<double*>(vptr);
	std::cout << *radius << std::endl;
	std::cout << sizeof(*static_cast<Circle*>(circle)) << std::endl;
	
	info(*static_cast<Circle*>(circle));


    return 0;
}

