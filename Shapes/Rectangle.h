#pragma once
#include "Shape.h"
#include <string>
using std::string;

class Rectangle :
	public Shape
{
	double a, b;
public:
	Rectangle(double a, double b);
	~Rectangle();

	virtual double area();
	virtual double perim();
protected:
	Rectangle(string n, double a, double b);
};

