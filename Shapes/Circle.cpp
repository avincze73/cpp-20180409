#include "Circle.h"
#define _USE_MATH_DEFINES
#include <math.h>


Circle::Circle(double radius)
	:Shape("CIRCLE"), radius(radius)
{
}


Circle::~Circle()
{
}

double Circle::area()
{
	return radius*radius*M_PI;
}

double Circle::perim()
{
	return 2*radius*M_PI;
}
