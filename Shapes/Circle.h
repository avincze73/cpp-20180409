#pragma once
#include "Shape.h"
class Circle :
	public Shape
{
	double radius;
public:
	Circle(double radius);
	~Circle();

	virtual double area();
	virtual double perim();
};

