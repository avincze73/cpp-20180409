#include "Rectangle.h"



Rectangle::Rectangle(double a, double b)
	:Shape("RECTANGLE"), a(a), b(b)
{
}


Rectangle::~Rectangle()
{
}

double Rectangle::area()
{
	return a*b;
}

double Rectangle::perim()
{
	return 2*(a+b);
}

Rectangle::Rectangle(string n, double a, double b)
	:Shape(n), a(a), b(b)
{
}
