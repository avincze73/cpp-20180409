#include "Time.h"

Time::Time()
	:hour(new int(0)),
	min(new int(0)),
	sec(new int(0))
{
}

Time::~Time()
{
	delete hour;
	delete min;
	delete sec;
}

Time::Time(int hour, 
	int min, int sec)
	:hour(new int(hour)),
	min(new int(min)),
	sec(new int(sec))
{
}


void Time::settime(int actual)
{
	*hour = actual / 3600;
	actual = actual%3600;
	*min = actual / 60;
	actual= actual%60;
	*sec=actual;
}

Time::operator int() const
{
	return *hour*3600 + *min*60 + *sec;
}


Time& Time::operator++()
{
	int actual = *this;
	actual++;
	settime(actual);
	return *this;
}

const Time Time::operator++(int)
{
	Time temp = *this;
	++*this;
	return temp;
}


Time::Time(const Time& cc)
	:hour(new int(*cc.hour)),
	min(new int(*cc.min)),
	sec(new int(*cc.sec))
{
}

Time::Time(int sec)
	:hour(new int(0)),
	min(new int(0)),
	sec(new int(0))
{
	settime(sec);
}

const Time Time::operator+(const Time& t)
{
	int t1 = *this;
	int actual = t1 + t;
	return Time(actual);
}

Time& Time::operator=(const Time& t)
{
	*hour = *t.hour;
	*min = *t.min;
	*sec = *t.sec;
	return *this;
}

int& Time::operator[](int index)
{
	switch(index)
	{
	case 0: return *hour;
	case 1: return *min;
	default: return *sec;
	}
}

int Time::operator==(const Time& t)
{
	int actual = t;
	return (int)*this == actual;
}

Time* Time::operator->()
{
	return this;
}

void Time::operator()(int actual)
{
	settime(actual);
}

std::ostream& operator<<(
	std::ostream& os,
	const Time& time)
{
	return os << "[" << *(time.hour) <<
		"," << *(time.min) << "," <<
		*(time.sec) << "]";

}

