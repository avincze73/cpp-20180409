// Operator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Time.h"
#include <iostream>
using namespace std;


void f(int& a)
{
	cout << "f(int& a)" << endl;
}


void f(int&& a)
{
	cout << "f(int&& a)" << endl;
}

int main()
{
	int a = 10;
	f(a);
	f(10);

	Time t(1, 59, 59);
	cout << t << endl;
	Time t2 = t++;
	Time* p1 = new Time();
	delete p1;
	//	printf("%s\n", typeid(*p1).name());

	t2 = t;
	t(61);
	t->show();


	Time* p = new Time(1, 2, 3);
	p = new Time(1, 2, 3);
	p = new Time(1, 2, 3);

	delete p;


    return 0;
}

