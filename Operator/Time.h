#pragma once
#include <ostream>
class Time
{
private:
	int* hour;
	int* min;
	int* sec;
	void settime(int actual);
public:
	Time();
	Time(int, int, int);
	Time(int);
	Time(const Time&);
	virtual ~Time();

	Time& operator++();
	const Time operator++(int);
	operator int() const;
	const Time operator+(const Time&);
	Time& operator=(const Time&);
	int& operator[](int);
	int operator==(const Time&);

	Time* operator->();
	void operator()(int);
	void show(){}
	friend std::ostream& operator<<(std::ostream&,
		const Time&);
};

std::ostream& operator<<(std::ostream&,
	const Time&);

