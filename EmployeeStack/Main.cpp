// EmployeeStack.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Employee.h"
#include "EmployeeStack.h"
#include <iostream>
#include "Header.h"
using namespace std;

int main()
{

	A<int> aa;
	aa.push(1);

	A<double> dd;

	B<int, int> b;
	
	//for (size_t i = 0; i < 20; i++)
	//{
	//	//EmployeeStack::getInstance()->push(Employee(i, "name","title"));
	//}

	//for (size_t i = 0; i < 20; i++)
	//{
	///*	Employee emp = EmployeeStack::getInstance()->pop();
	//	std::cout << emp.getAge() << " " 
	//		<< emp.getName() << " " <<
	//		emp.getTitle() << std::endl;*/
	//}


	GenericStack<int> gs1;
	GenericStack<Employee> gs2;
	for (size_t i = 0; i < 30; i++)
	{
		gs1.push(i);
		gs2.push(Employee(i, "a", "b"));
	}

	for (size_t i = 0; i < 30; i++)
	{
		cout << gs1.pop() << endl;
		cout << gs2.pop().getAge() << endl;
	}

    return 0;
}

