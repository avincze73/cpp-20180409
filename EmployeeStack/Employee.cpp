#include "Employee.h"
#include <string>


Employee::Employee()
	:age(30), title(new char[2])
{
	std::strcpy(title, "");
	std::strcpy(name, "");
}

Employee::Employee(int age, const char* name, const char* title)
	:age(age), title(new char[std::strlen(title) + 1])
{
	std::strcpy(this->title, title);
	std::strcpy(this->name, name);
}

Employee::Employee(const Employee & v)
	:age(v.age), title(new char[std::strlen(v.title) + 1])
{
	std::strcpy(this->title, v.title);
	std::strcpy(this->name, v.name);
}

Employee & Employee::operator=(const Employee & v)
{
	this->age = v.age;
	std::strcpy(this->name, v.name);
	delete[] this->title;
	this->title = new char[std::strlen(v.title) + 1];
	std::strcpy(this->title, v.title);
	return *this;
}

int Employee::getAge()
{
	return age;
}

void Employee::setAge(int age)
{
	this->age = age;
}

const char* Employee::getName()
{
	char* p 
		= new char[std::strlen(this->name) + 1];
	std::strcpy(p, this->name);
	return p;
}

void Employee::setName(const char* name)
{
	std::strcpy(this->name, name);
}


const char* Employee::getTitle()
{
	return this->title;
}

void Employee::setTitle(const char* title)
{
	delete this->title;
	this->title = new char[std::strlen(title) + 1];
	std::strcpy(this->title, title);
}
