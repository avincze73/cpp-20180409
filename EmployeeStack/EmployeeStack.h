#pragma once
#include "Employee.h"
class EmployeeStack
{
private:
	EmployeeStack();
public:
	static EmployeeStack* getInstance();
	void push(Employee);
	Employee pop();
};

