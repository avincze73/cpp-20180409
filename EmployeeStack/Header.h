#pragma once
#include <type_traits>

template <typename T>
class GenericStack
{
private:
	T* stack;
	int top;
	int size;
public:
	GenericStack();
	void push(T);
	T pop();
};

template <typename T>
GenericStack<T>::GenericStack()
	:top(0), size(0), stack(new T[0])
{
}

template <typename T>
void GenericStack<T>::push(T arg)
{
	if (top == size)
	{
		int newSize = 2 * size + 1;
		T* temp = new T[newSize];
		for (size_t i = 0; i < size; i++)
		{
			temp[i] = i[stack];
		}
		delete[] stack;

		size = newSize;
		stack = temp;
	}
	stack[top++] = arg;

}

template <typename T>
T GenericStack<T>::pop()
{
	return stack[top--];
}


template <typename T>
class A;

template <>
class A<int>
{
public:
	void push(int a) {

	}
};


template <>
class A<double>
{
public:
	void push(double a) {

	}
};

template <typename T, typename K>
class B
{
	//static_assert(std::is_floating_point<K>::value,
	//	"Floating point requires...");
};