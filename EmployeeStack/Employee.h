#pragma once
class Employee
{
private:
	int age;
	char name[50];
	char* title;
public:
	Employee();
	Employee(int age, const char* name, const char* title);
	 Employee(const Employee&);
	~Employee()
	{
		delete[] title;
	}
	Employee& operator=(const Employee&);
	int getAge();
	void setAge(int age);
	const char* getName();
	void setName(const char* name);
	const char* getTitle();
	void setTitle(const char* title);

};

