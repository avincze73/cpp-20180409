#include "EmployeeStack.h"

//https://gitlab.com/avincze73/cpp-20180409

static Employee* container = 0;
static int top = 0;
static int size = 0;

EmployeeStack::EmployeeStack()
{
}

EmployeeStack * EmployeeStack::getInstance()
{
	static EmployeeStack* onlyInstance = new EmployeeStack;
	return onlyInstance;
}

void EmployeeStack::push(Employee arg)
{
	if (top == size)
	{
		int newSize = 2 * size + 1;
		Employee* temp = new Employee[newSize];
		for (size_t i = 0; i < size; i++)
		{
			temp[i] = i[container];
		}
		delete[] container;
		
		size = newSize;
		container = temp;
	}
	container[top++] = arg;
}

Employee EmployeeStack::pop()
{
	//return container[--top];

	return Employee(1,"","");
}


